const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = 5000;
const mongoose = require('mongoose');
const productRoute = require('./controllers/ProductRoute');
const MONGOURI = 'mongodb+srv://root:Vivek07$@cluster0-wemqu.mongodb.net/products';

app.use(bodyParser.json());
app.use('/api',productRoute);

mongoose.connect(MONGOURI,{useNewUrlParser:true,useCreateIndex :true}).then(()=>{
    console.log("Connected to Mongo database")
})

app.listen(PORT,()=>{
    console.log("App is running on server",PORT);
})

